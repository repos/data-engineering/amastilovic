# SQL Linter and HDFS Deployment pipeline test playground

## Running the linter

To create a container and run linter on files in folder `hql`:

`docker build -f .pipeline/blubber.yaml --tag lint-py --target lint-py ./python`

To run the `lint-py` container on a directory of files on your local
file system and fix them according to the rules, run the following
command after replacing

`DIR_WITH_SQLFLUFF_CONFIG_AND_SQL_FILES` with a path to the folder
on local disk containing this repository.

``` shell
docker run \
  --platform linux/x86_64 \
  --memory="8g" \
  --cpus="5.0" \
  --rm -it \
  --volume ./DIR_WITH_SQLFLUFF_CONFIG_AND_SQL_FILES:/srv/app \
  --workdir /srv/app \
  --entrypoint bash \
  lint-py \
  -c -p "sqlfluff lint hql"
```

You can also run the `sqlfluff fix` command, to apply the automatic linting
fix where possible:

``` shell
docker run \
  --platform linux/x86_64 \
  --memory="8g" \
  --cpus="5.0" \
  --rm -it \
  --volume ./DIR_WITH_SQLFLUFF_CONFIG_AND_SQL_FILES:/srv/app \
  --workdir /srv/app \
  --entrypoint bash \
  lint-py \
  -c -p "sqlfluff fix hql"
```
