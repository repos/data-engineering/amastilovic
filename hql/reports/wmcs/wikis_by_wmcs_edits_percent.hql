-- Parameters:
--     source_table         -- Fully qualified table name to compute the
--                             aggregation for.
--     destination_dir      -- HDFS directory path where the query results file will
--                             be saved in
--     agg_results_path     -- Path to the resulting aggregate report TSV file
--     start_date           -- Starting date of the computation
--     end_date             -- End date of the computation (start_date + 7 days)
--
-- Usage:
-- spark3-sql -f wikis_by_wmcs_edits_percent.hql                                        \
--            -d source_table=wmf.editors_daily     \
--            -d destination_dir=/wmf/tmp/analytics/reports/wmcs/wikis_by_wmcs_edits_percent
--            -d agg_results_path=/wmf/data/published/datasets/periodic/reports/wmcs/wikis_by_wmcs_edits_percent.tsv    \
--            -d start_date=20240212                                \
--            -d end_date=20240219

CREATE TEMPORARY VIEW agg_results_view
USING CSV
OPTIONS (
    'path' '${agg_results_path}',
    'header' 'true',
    'delimiter' '\t',
    'inferSchema' 'true'
);

INSERT OVERWRITE DIRECTORY '${destination_dir}'
USING CSV
OPTIONS (
    'sep' '\t',
    'header' 'true',
    'compression' 'none'
)
WITH slice AS (
    SELECT
        editors.wiki_db,
        SUM(IF(editors.network_origin = 'wikimedia_labs', editors.edit_count, 0)) AS wmcs_edits,
        SUM(editors.edit_count) AS total_edits,
        ROUND(
            SUM(IF(editors.network_origin = 'wikimedia_labs', editors.edit_count, 0)) / SUM(editors.edit_count), 3
        ) AS wmcs_percent
    FROM ${source_table} AS editors
    WHERE
        editors.month = SUBSTR('${start_date}', 1, 7)
    GROUP BY
        editors.wiki_db
),

current_results AS (
    SELECT
        '${start_date}' AS `date`,
        'TOTAL' AS wiki_db,
        ROUND(SUM(slice.wmcs_edits) / SUM(slice.total_edits), 3) AS wmcs_percent
    FROM slice

    UNION ALL

    SELECT
        '${start_date}' AS `date`,
        slice.wiki_db,
        slice.wmcs_percent
    FROM slice
    ORDER BY
        slice.wiki_db
    LIMIT 10000
),

total_agg AS (
    SELECT *
    FROM agg_results_view
    WHERE
        agg_results_view.`date` != '${start_date}'

    UNION ALL

    SELECT *
    FROM current_results
)

SELECT /*+ COALESCE(1) */ *
FROM total_agg
ORDER BY
    total_agg.`date` ASC,
    total_agg.wiki_db ASC;
