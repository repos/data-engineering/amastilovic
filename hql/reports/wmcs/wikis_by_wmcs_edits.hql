-- Parameters:
--     source_table         -- Fully qualified table name to compute the
--                             aggregation for.
--     destination_dir      -- HDFS directory path where the query results file will
--                             be saved in
--     agg_results_path     -- Path to the resulting aggregate report TSV file
--     start_date           -- Starting date of the computation
--     end_date             -- End date of the computation (start_date + 7 days)
--
-- Usage:
-- spark3-sql -f wikis_by_wmcs_edits.hql                                                \
--            -d source_table=wmf.editors_daily          \
--            -d destination_dir=/wmf/tmp/analytics/reports/wmcs/wikis_by_wmcs_edits       \
--            -d agg_results_path=/wmf/data/published/datasets/periodic/reports/wmcs/wikis_by_wmcs_edits.tsv    \
--            -d start_date=20240212                                     \
--            -d end_date=20240219

CREATE TEMPORARY VIEW agg_results_view
USING CSV
OPTIONS (
    'path' '${agg_results_path}',
    'header' 'true',
    'delimiter' '\t',
    'inferSchema' 'true'
);

INSERT OVERWRITE DIRECTORY '${destination_dir}'
USING CSV
OPTIONS (
    'sep' '\t',
    'header' 'true',
    'compression' 'none'
)

WITH current_results AS (
    SELECT
        '${start_date}' AS `date`,
        editors.wiki_db,
        SUM(IF(editors.network_origin = 'wikimedia_labs', editors.edit_count, 0)) AS wmcs_edits
    FROM ${source_table} AS editors
    WHERE
        editors.month = SUBSTR('${start_date}', 1, 7)
    GROUP BY
        editors.wiki_db
    ORDER BY
        editors.wiki_db
    LIMIT 10000

),

total_agg AS (
    SELECT *
    FROM agg_results_view
    WHERE
        agg_results_view.`date` != '${start_date}'

    UNION ALL

    SELECT *
    FROM current_results
)

SELECT /*+ COALESCE(1) */ *
FROM total_agg
ORDER BY
    total_agg.`date` ASC,
    total_agg.wiki_db ASC;
