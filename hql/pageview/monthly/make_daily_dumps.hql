-- Transform a month of pageview data into a zipped tsv file,
-- each row containing daily values encoded in Pagecounts-EZ
-- format.  Can only do this for one agent type at a time.
--
-- Parameters:
--     agent_type               -- Filter to only this agent type
--     source_table             -- Fully qualified table name to get pageviews from.
--     destination_directory    -- Where to put the generated file
--     year
--     month

-- Usage:
-- spark3-sql -f make_daily_dumps.hql                           \
--      --master yarn --executor-cores 2 --executor-memory 1G   \
--      --conf spark.dynamicAllocation.maxExecutors=64          \
--      --conf spark.executor.memoryOverhead=7G                 \
--      -d agent_type=user                                      \
--      -d source_table=wmf.pageview_hourly                     \
--      -d destination_directory=hdfs:///user/milimetric/test_m \
--      -d year=2023                                            \
--      -d month=1

-- Note:
--   We set low executor memory (1G) and high overhead (7G) because the executors mostly
--   read/aggregate and then write, with the exception of the last stage which does a
--   big sort.  So the minimum executor memory lets everything get by without too much
--   allocation, and the overhead enables the sort.

SET spark.sql.shuffle.partitions = 1024;

INSERT OVERWRITE DIRECTORY '${destination_directory}'
USING CSV
OPTIONS ('compression' 'bzip2', 'sep' ' ')

-- Note: contrary to what spark docs say about the REPARTITION hint, it seems that it
--   ignores any parameters.  In our trials, REPARTITION(128) here had the same effect
--   as plain REPARTITION with the explicit set of shuffle.partitions above.  Without
--   that explicit set, REPARTITION ignores parameters and Spark uses 200 (the default)
WITH pre_aggregated AS (
    SELECT /*+ REPARTITION*/
        project,
        page_title,
        COALESCE(CAST(page_id AS string), 'null') AS page_id,
        REGEXP_REPLACE(access_method, ' ', '-') AS access_method,
        CAST(SUM(view_count) AS string) AS sum_view_count,
        REGEXP_REPLACE(CONCAT(
            CONCAT('A', CAST(SUM(IF(day = 1, view_count, 0)) AS string)),
            CONCAT('B', CAST(SUM(IF(day = 2, view_count, 0)) AS string)),
            CONCAT('C', CAST(SUM(IF(day = 3, view_count, 0)) AS string)),
            CONCAT('D', CAST(SUM(IF(day = 4, view_count, 0)) AS string)),
            CONCAT('E', CAST(SUM(IF(day = 5, view_count, 0)) AS string)),
            CONCAT('F', CAST(SUM(IF(day = 6, view_count, 0)) AS string)),
            CONCAT('G', CAST(SUM(IF(day = 7, view_count, 0)) AS string)),
            CONCAT('H', CAST(SUM(IF(day = 8, view_count, 0)) AS string)),
            CONCAT('I', CAST(SUM(IF(day = 9, view_count, 0)) AS string)),
            CONCAT('J', CAST(SUM(IF(day = 10, view_count, 0)) AS string)),
            CONCAT('K', CAST(SUM(IF(day = 11, view_count, 0)) AS string)),
            CONCAT('L', CAST(SUM(IF(day = 12, view_count, 0)) AS string)),
            CONCAT('M', CAST(SUM(IF(day = 13, view_count, 0)) AS string)),
            CONCAT('N', CAST(SUM(IF(day = 14, view_count, 0)) AS string)),
            CONCAT('O', CAST(SUM(IF(day = 15, view_count, 0)) AS string)),
            CONCAT('P', CAST(SUM(IF(day = 16, view_count, 0)) AS string)),
            CONCAT('Q', CAST(SUM(IF(day = 17, view_count, 0)) AS string)),
            CONCAT('R', CAST(SUM(IF(day = 18, view_count, 0)) AS string)),
            CONCAT('S', CAST(SUM(IF(day = 19, view_count, 0)) AS string)),
            CONCAT('T', CAST(SUM(IF(day = 20, view_count, 0)) AS string)),
            CONCAT('U', CAST(SUM(IF(day = 21, view_count, 0)) AS string)),
            CONCAT('V', CAST(SUM(IF(day = 22, view_count, 0)) AS string)),
            CONCAT('W', CAST(SUM(IF(day = 23, view_count, 0)) AS string)),
            CONCAT('X', CAST(SUM(IF(day = 24, view_count, 0)) AS string)),
            CONCAT('Y', CAST(SUM(IF(day = 25, view_count, 0)) AS string)),
            CONCAT('Z', CAST(SUM(IF(day = 26, view_count, 0)) AS string)),
            CONCAT('[', CAST(SUM(IF(day = 27, view_count, 0)) AS string)),
            CONCAT('\\', CAST(SUM(IF(day = 28, view_count, 0)) AS string)),
            CONCAT(']', CAST(SUM(IF(day = 29, view_count, 0)) AS string)),
            CONCAT('^', CAST(SUM(IF(day = 30, view_count, 0)) AS string)),
            CONCAT('_', CAST(SUM(IF(day = 31, view_count, 0)) AS string))
        ), '([A-Z]|[\\[\\]\\\\^\\_])0', '') AS view_count_per_hour
    FROM ${source_table}
    WHERE
        year = ${year}
        AND month = ${month}
        AND agent_type = '${agent_type}'
    GROUP BY
        project, page_title,
        COALESCE(CAST(page_id AS string), 'null'), REGEXP_REPLACE(access_method, ' ', '-')
)

-- coalesce 1 to get a single file as the output, for easy archiving
SELECT /*+ COALESCE(1) */ *
FROM pre_aggregated
ORDER BY project, page_title;
