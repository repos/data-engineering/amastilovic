-- Aggregate editors_daily data into monthly summaries of unique editor counts by country, regardless of wiki project.
-- Note: This dataset does NOT contain bots actions and only considers edit actions
--
-- Parameters:
--     source_table         -- Read raw data from here
--     destination_table    -- Insert results here
--     month                -- YYYY-MM to compute statistics for
--     coalesce_partitions  -- Number of partitions to write
--
-- Usage:
--     spark3-sql -f unique_editors_by_country_monthly.hql         \
--                -d source_table=wmf.editors_daily             \
--                -d destination_table=wmf.unique_editors_by_country_monthly   \
--                -d month=2022-02    \
--                -d coalesce_partitions=1
--
-- NOTE: as discussed in https://phabricator.wikimedia.org/T324907#8541865,
--   entries with cuc_type = 3 are being moved into separate tables
--   currently these are not used downstream, but if they become useful, hopefully
--   someone finds this comment

WITH overall AS (

    SELECT
        country_code,
        user_is_anonymous AS users_are_anonymous,
        activity_level,
        COUNT(*) AS distinct_editors
    FROM (
        SELECT
            country_code,
            user_is_anonymous,
            user_fingerprint_or_name,
            CASE
                WHEN SUM(edit_count) >= 100 THEN '100 or more'
                WHEN SUM(edit_count) >= 5 THEN '5 to 99'
                ELSE '1 to 4'
            END AS activity_level
        FROM ${source_table}
        WHERE
            month = '${month}'
            -- Filter out bot actions and non-edit actions
            AND SIZE(user_is_bot_by) = 0
            AND action_type IN (0, 1)
        GROUP BY
            country_code,
            user_is_anonymous,
            user_fingerprint_or_name
    ) AS editors_with_monthly_activity
    GROUP BY
        country_code,
        activity_level,
        user_is_anonymous

),

only_ns0 AS (

    SELECT
        country_code,
        user_is_anonymous AS users_are_anonymous,
        activity_level,
        COUNT(*) AS distinct_editors

    FROM (
        SELECT
            country_code,
            user_is_anonymous,
            user_fingerprint_or_name,
            CASE
                WHEN SUM(namespace_zero_edit_count) >= 100 THEN '100 or more'
                WHEN SUM(namespace_zero_edit_count) >= 5 THEN '5 to 99'
                ELSE '1 to 4'
            END AS activity_level
        FROM ${source_table}
        WHERE
            month = '${month}'
            -- Filter out bot actions, non-edit actions
            AND SIZE(user_is_bot_by) = 0
            AND action_type IN (0, 1)
            -- Filter out rows having 0 namespace-zero actions
            AND namespace_zero_edit_count > 0
        GROUP BY
            country_code,
            user_is_anonymous,
            user_fingerprint_or_name
    ) AS editors_with_monthly_activity

    GROUP BY
        country_code,
        activity_level,
        user_is_anonymous

)

INSERT OVERWRITE TABLE ${destination_table}
PARTITION (month = '${month}')

SELECT /*+ COALESCE(${coalesce_partitions}) */
    COALESCE(overall.country_code, only_ns0.country_code) AS country_code,
    COALESCE(overall.users_are_anonymous, only_ns0.users_are_anonymous) AS users_are_anonymous,
    COALESCE(overall.activity_level, only_ns0.activity_level) AS activity_level,
    COALESCE(overall.distinct_editors, 0) AS distinct_editors,
    COALESCE(only_ns0.distinct_editors, 0) AS namespace_zero_distinct_editors
FROM overall
FULL OUTER JOIN only_ns0 ON
    overall.country_code = only_ns0.country_code
    AND overall.users_are_anonymous = only_ns0.users_are_anonymous
    AND overall.activity_level = only_ns0.activity_level;
