-- Aggregate editors_daily data into edits by country and other dimensions
-- Note: This dataset does NOT contain bots actions and only considers edit actions.
--
-- Parameters:
--     source_table         -- Read raw data from here
--     destination_table    -- Insert results here
--     month                -- YYYY-MM to compute statistics for
--     coalesce_partitions  -- Number of partitions to write
--
-- Usage:
--     hive -f geoeditors_edits_monthly.hql        \
--          -d source_table=wmf.editors_daily                  \
--          -d destination_table=wmf.geoeditors_edits_monthly  \
--          -d month=2022-02
--          -d coalesce_partitions=1
--
-- NOTE: as discussed in https://phabricator.wikimedia.org/T324907#8541865,
--   entries with cuc_type = 3 are being moved into separate tables
--   In this job, action_type IN (0, 1) below means we don't use type 3,
--   but if this becomes useful in the future, it'll need an update here.

INSERT OVERWRITE TABLE ${destination_table}
PARTITION (month = '${month}')
SELECT /*+ COALESCE(${coalesce_partitions}) */
    wiki_db,
    country_code,
    user_is_anonymous AS edits_are_anonymous,
    SUM(edit_count) AS edit_count,
    SUM(namespace_zero_edit_count) AS namespace_zero_edit_count
FROM ${source_table}
WHERE
    month = '${month}'
    -- Filter out bot actions and non-edit actions
    AND SIZE(user_is_bot_by) = 0
    AND action_type IN (0, 1)
GROUP BY
    wiki_db,
    country_code,
    user_is_anonymous;
