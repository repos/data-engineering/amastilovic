-- Aggregate geoeditors_edits_monthly data into edits by country by year.
--   Do not show any countries with fewer than 100,000 edits per year
--   for privacy reasons.
--
-- Parameters:
--      project_map_table       -- Read project info from here
--      project_map_table_snapshot
--                              -- The partition to use from project_map_table
--      country_map_table       -- Read country info from here
--      source_table            -- Read monthly editors data from here
--      destination_directory   -- Write yearly files here
--      namespace_zero_edit_count_threshold
--                              -- The threshold of namespace-zero-edits over
--                              -- which a country is included in the report
--      year                    -- YYYY to compute statistics for
--      project_family          -- The project family for which the report is computed
--
-- Usage:
-- spark-sql  -f write_geoeditors_edits_yearly_data.hql                             \
--            -d project_map_table=wmf_raw.mediawiki_project_namespace_map          \
--            -d project_map_table_snapshot=2021-12                                 \
--            -d country_map_table=canonical_data.countries                         \
--            -d source_table=wmf.geoeditors_edits_monthly                          \
--            -d destination_directory=/wmf/tmp/analytics/geoeditors/edits/yearly   \
--            -d namespace_zero_edit_count_threshold=100000                         \
--            -d year=2021                                                          \
--            -d project_family=wikipedia
--
-- Note: This SQL script only works on Spark 3.X.

INSERT OVERWRITE DIRECTORY '${destination_directory}'
USING CSV
OPTIONS ('header' 'false', 'compression' 'none')

WITH proj_map AS (
    SELECT DISTINCT dbname
    FROM ${project_map_table}
    WHERE
        hostname LIKE '%.${project_family}.%'
        AND snapshot = '${project_map_table_snapshot}'
),

output AS (
    SELECT
        c.name AS country,
        SUM(edit_count) AS edits,
        SUM(namespace_zero_edit_count) AS namespace_zero_edits

    FROM ${source_table} AS g
    INNER JOIN proj_map AS w ON g.wiki_db = w.dbname
    INNER JOIN
        ${country_map_table} AS c ON g.country_code = c.iso_code

    WHERE month LIKE '${year}-%'

    GROUP BY c.name
    HAVING SUM(namespace_zero_edit_count) >= ${namespace_zero_edit_count_threshold}
    ORDER BY country
    LIMIT 10000
)


SELECT /*+ COALESCE(1) */
    country,
    edits,
    namespace_zero_edits
FROM output;
