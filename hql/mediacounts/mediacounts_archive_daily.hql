--Archives mediacounts data for upload webrequest_source
--
-- Parameters:
--     destination_directory -- Directory in HDFS where to store the generated
--                              data in.
--     source_table          -- table containing pre-aggregated hourly
--                              mediacounts data
--     year                  -- year of the date to archive for
--     month                 -- month of the date to archive for
--     day                   -- day of the date to archive for
--
--
-- Usage:
--     spark-sql -f mediacounts_archive_daily.hql       \
--         -d destination_directory=/wmf/tmp/analytics/foo \
--         -d source_table=wmf.mediacounts   \
--         -d year=2022                      \
--         -d month=12                        \
--         -d day=01


INSERT OVERWRITE DIRECTORY "${destination_directory}"
USING CSV OPTIONS ('sep' = "\t", 'compression' = "bzip2")
SELECT /*+ COALESCE(1) */
    base_name,
    "-" AS reserved_1,
    "-" AS reserved_2,
    "-" AS reserved_3,
    "-" AS reserved_4,
    "-" AS reserved_5, -- reserved for future use
    "-" AS reserved_6, -- reserved for future use
    SUM(total_response_size) AS total_response_size,
    SUM(total) AS total,
    SUM(original) AS original,
    SUM(transcoded_audio) AS transcoded_audio,
    SUM(transcoded_image) AS transcoded_image,
    SUM(transcoded_image_0_199) AS transcoded_image_0_199,
    SUM(transcoded_image_200_399) AS transcoded_image_200_399,
    SUM(transcoded_image_400_599) AS transcoded_image_400_599, -- reserved for future use
    SUM(transcoded_image_600_799) AS transcoded_image_600_799, -- reserved for future use
    SUM(transcoded_image_800_999) AS transcoded_image_800_999,
    SUM(transcoded_image_1000) AS transcoded_image_1000,
    SUM(transcoded_movie) AS transcoded_movie,
    SUM(transcoded_movie_0_239) AS transcoded_movie_0_239,
    SUM(transcoded_movie_240_479) AS transcoded_movie_240_479, -- reserved for future use
    SUM(transcoded_movie_480) AS transcoded_movie_480, -- reserved for future use
    SUM(referer_internal) AS referer_internal,
    SUM(referer_external) AS referer_external,
    SUM(referer_unknown) AS referer_unknown
FROM ${source_table}
WHERE
    year = ${year}
    AND month = ${month}
    AND day = ${day}
GROUP BY base_name
ORDER BY base_name;
