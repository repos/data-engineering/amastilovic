-- Parameters:
--  refinery_hive_shaded
--  source_table         -- Fully qualified table name to compute the
--                          aggregation for.
--  destination_table    -- Fully qualified table name to fill in
--                          aggregated values.
--  coalesce_partitions  -- Number of files in the result
--  year                 -- year of partition to compute aggregation for.
--  month                -- month of partition to compute aggregation for.
--  day                  -- day of partition to compute aggregation for.
--  hour                 -- hour of partition to compute aggregation for.
--
-- Usage:
-- spark3-sql
--         --name=mediarequest_hourly_manual \
--         --master=yarn \
--         -f mediarequest_hourly.hql \
--         -d refinery_hive_shaded=hdfs://analytics-hadoop/wmf/refinery/current/artifacts/refinery-hive-shaded.jar \
--         -d source_table=wmf.webrequest                                 \
--         -d destination_table=wmf.mediarequest                          \
--         -d coalesce_partitions=64                                      \
--         -d year=2021                                                   \
--         -d month=2                                                     \
--         -d day=9                                                       \
--         -d hour=6
--
SET parquet.compression = 'SNAPPY';

ADD JAR ${refinery_hive_shaded};
CREATE TEMPORARY FUNCTION PARSE_MEDIA_FILE_URL AS 'org.wikimedia.analytics.refinery.hive.GetMediaFilePropertiesUDF';
CREATE TEMPORARY FUNCTION CLASSIFY_REFERER AS 'org.wikimedia.analytics.refinery.hive.GetRefererTypeUDF';
CREATE TEMPORARY FUNCTION REFERER_WIKI AS 'org.wikimedia.analytics.refinery.hive.GetRefererWikiUDF';

INSERT OVERWRITE TABLE ${destination_table}
PARTITION (year = '${year}', month = '${month}', day = '${day}', hour = '${hour}')
WITH upload_webrequests AS (
    SELECT
        response_size,
        agent_type,
        PARSE_MEDIA_FILE_URL(uri_path) AS parsed_url,
        REFERER_WIKI(referer) AS referer_wiki,
        CLASSIFY_REFERER(referer) AS classified_referer
    FROM ${source_table}
    WHERE
        webrequest_source = 'upload'
        AND year = ${year}
        AND month = ${month}
        AND day = ${day}
        AND hour = ${hour}
        AND uri_host = 'upload.wikimedia.org'
        AND (
            http_status = 200 -- No 304 per RFC discussion
            OR (
                http_status = 206
                AND SUBSTR(`range`, 1, 8) = 'bytes=0-'
                AND `range` != 'bytes=0-0'
            )
        )
)

SELECT /*+ COALESCE(${coalesce_partitions}) */
    parsed_url.base_name,
    parsed_url.media_classification,
    parsed_url.file_type,
    SUM(response_size) AS total_bytes,
    COUNT(*) AS request_count,
    parsed_url.transcoding,
    agent_type,
    IF(
        classified_referer = 'internal',
        COALESCE(referer_wiki, 'internal'),
        COALESCE(classified_referer, 'unknown')
    ) AS referer,
    CONCAT(
        LPAD(${year}, 4, '0'), '-',
        LPAD(${month}, 2, '0'), '-',
        LPAD(${day}, 2, '0'), 'T',
        LPAD(${hour}, 2, '0'),
        ':00:00Z'
    ) AS dt
FROM upload_webrequests
WHERE parsed_url.base_name IS NOT NULL
GROUP BY
    parsed_url.base_name,
    parsed_url.media_classification,
    parsed_url.file_type,
    IF(classified_referer = 'internal', COALESCE(referer_wiki, 'internal'), COALESCE(classified_referer, 'unknown')),
    parsed_url.transcoding,
    agent_type;
